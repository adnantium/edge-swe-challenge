
import lorem
import random
import time

from protobuf.generated import event_pb2, execution_pb2


def make_random_sport_event():
    event = event_pb2.event()

    event.sport = random.randint(1,7)
    event.match_title = lorem.paragraph()
    event.data_event = lorem.sentence()
    return event




SYMBOL_MARKET_MAP = {
    "DAL-PHL:Line": "FOOTBALL",
    "DAL-PHL:spread--5": "FOOTBALL",
    "DAL-PHL:points--40": "FOOTBALL",
    "MASTERS:winner--Tiger Woods": "GOLF",
    "MASTERS:winner--Rory McLlroy": "GOLF",
    "WIMBELDON:winner--Roger Federer": "TENNIS",
    "WIMBELDON:winner--Novak Djokovic": "TENNIS",
    "DAYTONA:winner--Bubba Wallace": "NASCAR",
    "DAYTONA:winner--Kyle Busch": "NASCAR",
    "OLYMPIC 100m:winner--Usain Bolt": "TRACK&FIELD",
    "OLYMPIC 100m:winner--Tyson Gay": "TRACK&FIELD",
    "PRESIDENT:winner--Donald Trump": "POLITICS",
    "PRESIDENT:winner--Joe Biden": "POLITICS",
    "PRESIDENT:winner--Vermin Supreme": "POLITICS"
    }

EXECUTION_TOPIC_NAME = "execution"



def make_random_execution():
    execution = execution_pb2.execution()

    execution.symbol, execution.market = random.choice(list(SYMBOL_MARKET_MAP.items()))
    execution.price = random.uniform(0, 100)
    execution.quantity = random.uniform(0, 1000)
    execution.stateSymbol = "PA"
    execution.executionEpoch = int(time.time())
    return execution
