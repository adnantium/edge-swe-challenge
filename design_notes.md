# SportTrade coding test

## Problem Definition 
* You are given two feeds running locally, sending messages to a local NATS broker. 
	* Catalog every message with an associated timestamp into a persistent storage. 
	* These messages are serialized via protobuf, and the spec is outlined in the repository below.
* Implement a chat room service that allows users to send and receive messages with each other. 
	* A room can have N number of users at any given time. 
	* This service must accept/send messages over websockets. 
	* This service must keep a durable record of events in a persistent storage.
	* Each room must have a set of filters for incoming sport and execution messages, blocking them from being disseminated to its participants.
	* Any user has the ability to update the room's preferences, given they have a valid session. 
	* Any incoming execution/sport data message will be sent to all room participants if it is not filtered out.
* Ensure exactly once delivery semantics for our users and persistent storage.
* You will find the feeds, NATS broker, and protobuf specs here: https://bitbucket.org/will-sumfest/edge-swe-challenge/src/master/

## Focus
* Demonstrate:
	* Systems design knowledge 
	* Coding skills
	* Documentation
	* Communication

## Core Functional Requirements
* 2 Feeds pumping out data
	* About 10 execution messages/second and 2 event messages per sec
	* Approx message size 80 bytes
* Every message coming thru the wire must be:
	* Saved in a persistent datastore
	* Processed by the ChatServers and shown to chatters
	* Preserving Message order is not a priority. Delivery is.
* Chat server must:
	* Capture every feed message received
	* Ensure every message gets sent to every user/room on the server that is authorized to get it (based of some room based filter logic)
	* Receive chat messages from chatter in a room
	* 
	* Send chat conversation messages from chatter to all other chatters in the room
	* Store every chat message from chatters in DB
	* 

## Design Choices
* Listen for feed messages by topic and worker queues.
	* NATS ensures that each feed message gets processed by 1 listener when sharing a worker queue
* Redis as primary datastore
	* Its quick and easy
	* Complex fast SQL querying is not a primary requirement
	* Data structures are semi structured with flexible schemas
	* Good for ingestion of fast moving data
	* Future:
		* It has async io support via [aioredis — aioredis 1.3.0 documentation](https://aioredis.readthedocs.io/) if needed
		* Clients can listen for new data events directly from DB rather than off of NATS
* Separate listeners for handling Event+Exec messages and associated processing logic vs. persisting stream data to DB
	* An independant listener for capturing and storing data coming across the wire.
		* Persistence of messages is a specialized capability that can be isolated to an independent component.
	* Chat server  needs 2-siding integration thru WebSockets and read/write from NATS
* 


## Questions 1
* Does the ordering of messages coming into the chat server be in sync with the order the are generated from the feeds? May get shuffled a little in ordering when going thru the pipes in failure scenarios
* 

### Logical Components
* Event / Execution Feeds
* NATS cluster
* 3 Feed Listeners
	1. Event Processor
	2. Execution Processor
	3. Feed Persister
* Chat Server
* Chat Clients (web server)


## System Design
* Consistency
* Resilience 
	* Containerization
* Availability
	* 
* Network Partition Tolerance
	* 
* Latency vs Throughput
* Overall Performance
* 


## Challenges
### Tech Unknowns
	* If server with a WebSock connection goes down, can another server pick up a reference to that WS connection and continue conversation with chatter?
		* Doesn’t sound doable. It is a TCP connection under the hood. It would be dead and unusable once the server (or the client) is gone.


### Design Questions

#### Chat server as a solo process???? 
	* Performance
		* WebSockets is hard to scale and still be resilient to failures
		* 
	* Failure:
		* When it goes down:
			* Events from Listener don’t get pushed to Chat Server
			* Chat server can’t push events to chatters 
		* When it comes back:
			* Rebuild its internal state 
				* and connections to chatters?
			* Catch up on events that it missed
				* send them to connected chatters
			* Get reconnects from clients
		* Data/Connections that will be lost:
			* Chat messages sent from chatters while server was down
			* All connections to clients will be dropped


#### Multiple listeners from NATS but only one Chat Server
	* NATS queue groups ensure each message only gets handled by 1 listener

#### Exactly Once Delivery??
	* Hard to do that without taking on a lot of complexity
	* Exactly once “processing” would be doable




## Failure Scenarios
* Feeds go die or go silent
	* Hard to know if down unless we have a heartbeat to listen for
	* NATS has monitoring hooks and ports for keeping eye on things like that
* NATS
	* Helpless. It not supposed to do that.
* Listeners:
		* Listen+Store Service:
			* How to pick up where we left off?
				* Can catch up from NATS?
					* No. Not an option with core NATS
				* Check DB?
					* May be able to detect that messages were missed but hard to get them again without NATS’s help
		* Listen+Forward Services
			* 
* Redis goes down:
	* Helpless. It not supposed to do that.
	* How to ensure missed data gets pushed in when it is back?
* Chat Server
	* Server process goes down
		* 
	* Connection to client(s) dropped
	* Connection from Listener is dropped
* Client gets disconnected
* Network failure scenarios

## Dev Notes
* “Awaitables” tasks:
	* listen for the next message published from the feed
	* read next incoming message from feed
	* read next incoming chatroom actions
	* send chatroom updates
	* write chat message to DB
* 

## Data structures
#### Chat client messages
JSON sent over a WebSocket to a chat server:
* `action``: 
	* `enter`: 
	* `chat`
	* `announcement`
* `room`: the name of the room that the action is occurring in
* `message`: The message text. Maybe be empty depending on `action`


### Dev Tasks

* Create server instances docker-compose
* Publish simple message to NATS
* Listen for simple message from NATS
* ~~Create Trade/Exec message as protobuf~~
* ~~Send protobuf message to NATS~~
* Listen for protobuf message from NATS
* Setup Redis instance
* Write message to Redis
* Send simple message to a WebSocket server
	* Establish secure authenticated connection
* Receive message from a WebSocket client
* Send message to multiple connected clients


#### Research Todo

- [ ] Websockets
	- [ ] [websockets — websockets 8.1 documentation](https://websockets.readthedocs.io/en/stable/)
	- [ ] [WebSockets - A Conceptual Deep Dive | Ably Realtime](https://www.ably.io/topic/websockets)
	- [ ] [GitHub - miguelgrinberg/Flask-SocketIO: Socket.IO integration for Flask applications.](https://github.com/miguelgrinberg/Flask-SocketIO)
	- [ ] [GitHub - aaugustin/websockets: Library for building WebSocket servers and clients in Python](https://github.com/aaugustin/websockets)
- [ ] Redis
	- [ ] [aioredis — aioredis 1.3.0 documentation](https://aioredis.readthedocs.io/)
- [ ] Async
	- [ ] [Python and Real-time Web |  Eat at Joe’s](http://mrjoes.github.io/2013/06/21/python-realtime.html) (from 2013 but still valid)
	- [ ] [Async IO in Python: A Complete Walkthrough – Real Python](https://realpython.com/async-io-python/)
- [ ] NATS
	- [ ] Streaming: [GitHub - nats-io/stan.py: Python Asyncio NATS Streaming Client](https://github.com/nats-io/stan.py)
	- [ ] [Deploy Secure and Scalable Service… Derek Collison, Colin Sullivan, Waldemar Quevedo, & Jaime Piña - YouTube](https://www.youtube.com/watch?v=aQHiFS3YSx4)


## Answered Questions
* ~~What should happen if a chat user gets disconnected (for whatever reason) ?~~
	* ~~Catch up on missed messages after reconnected?~~
	* ~~When chat user connects - get list of last N messages? Or just start~~ 
	* ~~Does the user get a history of the chat room’s conversions when connected (or reconnected)?~~
	* ~~Or does only gets new messages with any catchup?~~
* ~~I’ll focus on system integration and backend, not the chat UI.~~ 
	* ~~It will be text and command line based~~
* 


## Resources
### asyncio
* [Waiting in asyncio · Homepage of Hynek Schlawack](https://hynek.me/articles/waiting-in-asyncio/)
* [asyncio: We Did It Wrong  – roguelynn](https://www.roguelynn.com/words/asyncio-we-did-it-wrong/)
	* [asyncio in Practice: We Did It Wrong  – roguelynn](https://www.roguelynn.com/talks/asyncio-in-practice/)
	* [Advanced asyncio: Solving Real-world Production Problems  – roguelynn](https://www.roguelynn.com/talks/advanced-asyncio/)
* [I don’t understand Python’s Asyncio | Armin Ronacher’s Thoughts and Writings](https://lucumr.pocoo.org/2016/10/30/i-dont-understand-asyncio/)
* [Async IO in Python: A Complete Walkthrough – Real Python](https://realpython.com/async-io-python/)
* [Getting Started With Async Features in Python – Real Python](https://realpython.com/python-async-features/)
* 

### WebSockets
* [WebSockets for fun and profit - Stack Overflow Blog](https://stackoverflow.blog/2019/12/18/websockets-for-fun-and-profit/)
* [GitHub - sjkingo/websocket-chat: A proof-of-concept chat server/client in WebSockets](https://github.com/sjkingo/websocket-chat)

### NATS
