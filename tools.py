
import logging
logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s: %(message)s", datefmt="%H:%M:%S")

def log_feed_message(msg):
    """Convienience method for logging messages from NATS."""
    subject, reply, data = msg.subject, msg.reply, msg.data
    logging.debug(f"Received: [{subject}] [{reply if reply else '--' }]: [{data}]")

def build_uid(m, uid_fields):
    """Build a uid key for a message based on a set of field names"""
    key = '|'.join([str(getattr(m, f)) for f in uid_fields])
    # remove whitespace
    key = ''.join(key.split())
    return key

def build_execution_uid(m):
    """Build a uid key for an execution message"""
    uid_fields = ['symbol', 'market', 'price', 'quantity', 'executionEpoch', 'stateSymbol', ]
    return build_uid(m, uid_fields)

def build_sport_uid(m):
    """Build a uid key for an sport message"""
    uid_fields = ["sport", "match_title", "data_event"]
    return build_uid(m, uid_fields)

