# Chat server built with Websockets + asyncio + NATS 

## Overview

Demo implementation of a chat server that:
    * Communicates with client via WebSockets
    * Consumes and shares data to NATS queues
    * Persists data from NATS queues into Redis

### Components
* Chat Server: `chat/chat_server.py`
* Chat Client: `chat_client/index.html`
* Feed Saver: `feed_saver/feed_saver.py`
* Data Feed: `feeds/sport_event_feed.py` & `feeds/execution_feed.py`
    * ripped off from the 'wsumfest/sport-feed:v1' docker image
* NATS Server: 


## Setup

Need:
* NATS: `$ brew install -v nats-server`
    * or `$ docker run -d -p 4222:4222 --name=nats-server --network=sport nats:latest`
* Redis: `$ brew install -v nats-server`
    * or `$ docker run -d -p 6379:6379 --name=redis-server --network=sport redis:latest`
* Python 3.8+
* `pyenv` (or something like that)
    ```
    $ pyenv shell 3.8.5
    $ pyenv virtualenv sporttrade1
    ```
* `docker` & `docker-compose`

### Option 1: with docker-compose
```
$ docker-compose up
Creating network "edge-swe-challenge_sport" with driver "bridge"
Building chat-server
...
Successfully tagged edge-swe-challenge_chat-server:latest
...
Building chat-web
...
Successfully tagged edge-swe-challenge_chat-web:latest
...
Building feed-saver
...
Successfully tagged edge-swe-challenge_feed-saver:latest
...
Creating nats-server  ... done
Creating redis-server ... done
Creating chat-server  ... done
Creating feed-saver   ... done
Creating chat-web     ... done
Attaching to nats-server, redis-server, feed-saver, chat-server, chat-web

...

^CGracefully stopping... (press Ctrl+C again to force)
Stopping chat-web     ... done
Stopping chat-server  ... done
Stopping feed-saver   ... done
Stopping redis-server ... done
Stopping nats-server  ... done
```

### Option 2: Build+run images individually
```
$ docker build -t chat-server:v1 -f ./chat/Dockerfile .
$ docker build -t chat-web:v1 -f ./chat_client/Dockerfile .
$ docker build -t feed-saver:v1 -f ./feed_saver/Dockerfile .
$ docker build -t executions-feed:v1 -f ./feeds/Dockerfile-Executions .
$ docker build -t sports-feed:v1 -f ./feeds/Dockerfile-Sports .
```
```
$ docker network create --driver=bridge sport
$ docker run -d -p 4222:4222 --name=nats-server --network=sport nats:latest
$ docker run -d -p 6379:6379 --name=redis-server --network=sport redis:latest
$ docker run -d -p 6789:6789 --name=chat-server --network=sport chat-server:v1
$ docker run -d -p 8000:8000 --name=chat-web --network=sport chat-web:v1
$ docker run -d --name=feed-saver --network=sport feed-saver:v1
```

## Run it

### Enter chat room
* Go to: http://localhost:8000
* Pick a 'handle' and 'room'
* Chat

### Simulate a feed
* `$ python feeds/execution_feed.py` or `$ python feeds/sport_event_feed.py`
* Messages will show up in chat rooms


## Design

See: [Design Notes](design_notes.md)


## TODOs
* Implement 'rooms' functionality. Room variables & refs are only in there as a placeholder
* Restructure ChatServer (chat_server.py) into an object structure. Its a big mess for nested async methods
    * e.g. `nats_client = NATS()` is defined at top global level
    * 
* Better connection, shutdown and general failure handling
    * Connectivity issues show big ugly stacktraces.
    * ^C starts the shutdown process and usually works but hangs depending on where you catch it in its loop
    * 
* Unit tests for async code.
    * Need to figure this out. Can test functionality and logic with unit tests but not sure about testing 'asynchronicity'
* Better way of managing server host names, ports, subject names etc. 
    * Some are hardcoded! or based on checking environment variables with defaults.
* Add type hinting
* 


