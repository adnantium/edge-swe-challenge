#!/usr/bin/env python

import asyncio
import websockets
import json
import time
import sys

from google.protobuf.json_format import MessageToDict

from tests.utils import make_random_execution

def main(sleep_secs=10):
    async def feeder():
        uri = "ws://localhost:6789"
        async with websockets.connect(uri) as websocket:
            print(f'Connected to {websocket}')
            while True:
                e = make_random_execution()
                e_data = MessageToDict(e)
                feed_data = { 'action': 'feed', 'data': e_data}
                await websocket.send(json.dumps(feed_data))
                print(f"Sent: {feed_data}")
                # asyncio.sleep(10)
                time.sleep(sleep_secs)

    asyncio.get_event_loop().run_until_complete(feeder())

if __name__ == '__main__':
    print(sys.argv)
    sleep_secs = 10 if len(sys.argv) <= 1 else int(sys.argv[1])
    main(sleep_secs=sleep_secs)