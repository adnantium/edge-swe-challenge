#!/usr/bin/env python

import asyncio
import json
import logging
import websockets

logging.basicConfig()


clients = {}

async def notify_clients(message, connections=None):
    connections = connections or clients.keys()
    # message = json.dumps({"type": "users", "count": len(connections)})
    for c in connections:
        await c.send(message)
    # await asyncio.wait([c.send(message) for c in connections])


async def register_client(websocket):
    print(f'Enter [{websocket}] ({len(clients)} existing clients)')
    # await notify_users()


async def unregister_client(websocket):
    print(f'Exit: {websocket}')
    their_name = clients.get(websocket)
    if not their_name:
        return
    del clients[websocket]
    await notify_clients(their_name + ' has left the chat')




async def handle_message(websocket, path):

    # await register(websocket)

    print('New client', websocket)
    print('New client [{}] ({} existing clients)'.format(websocket, len(clients)))

    try:
        async for raw_message in websocket:
            print(f'Got: [{raw_message}]')

            # entry_data_json = await websocket.recv()
            # print(entry_data_json)
            message_data = json.loads(raw_message)

            action = message_data.get('action')
            if not action:
                print('"action" is missing from {message_data}')
                continue

            if action == 'enter':
                name = message_data['name']
                # TODO: check if already in room. then ignore?
                clients[websocket] = name

                print(f'Enter [{websocket}] ({len(clients)} existing clients)')

                await websocket.send('Welcome to websocket-chat, {}'.format(name))
                await websocket.send('There are {} other users connected: {}'.format(len(clients), list(clients.values())))
                
                await notify_clients(f'{name} has joined the chat')

            elif action == 'leave':
                their_name = clients[websocket]
                del clients[websocket]
                await notify_clients(f'{their_name} has left the chat')

            elif action == 'chat':
                # ensure the user is in the room?
                msg = message_data['message']
                name = clients[websocket]
                await notify_clients(f'{name}: {msg}')

            elif action == 'feed':
                data = message_data['data']
                await notify_clients(f'{action}: {data}')

            else:
                pass
    finally:
        await unregister_client(websocket)


start_server = websockets.serve(handle_message, "localhost", 6789)
print(f'Starting: {start_server}')

loop = asyncio.get_event_loop()
loop.run_until_complete(start_server)
try:
    loop.run_forever()
except KeyboardInterrupt as ki:
    print(ki)
    # raise ki
loop.close()
print(f'Shutting down: {start_server}')
