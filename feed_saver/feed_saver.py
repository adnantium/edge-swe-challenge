import asyncio
import os
import signal
import sys
import logging

from nats.aio.client import Client as NATS
import redis
from slugify import slugify

from protobuf.generated import event_pb2, execution_pb2
from tools import build_execution_uid, build_sport_uid, log_feed_message

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s: %(message)s", datefmt="%H:%M:%S")
"""Simple compeonents that listens for various data events coming across
the wire from NATS. Data is parsed/serialized/deser as needed , a unique 'key' 
is created and put into KV store.

Environments variables expected:
* NATS_HOSTNAME (default: localhost)
* REDIS_HOSTNAME (default: localhost)
"""

SPORT_EVENT_SUBJECT_NAME = "sport_event"
SPORT_EVENT_WORKERS_NAME = "sport_event_workers"
EXECUTION_SUBJECT_NAME = "execution"
EXECUTION_WORKERS_NAME = "execution_workers"

CHAT_EVENTS_SUBJECT_NAME = 'chatroom_event'
CHAT_EVENTS_WORKERS_NAME = 'chatroom_event_workers'

NATS_HOSTNAME = os.environ.get("NATS_HOSTNAME", "localhost")
NATS_SERVERS = [f"nats://{NATS_HOSTNAME}:4222"]

REDIS_HOSTNAME = os.environ.get("REDIS_HOSTNAME", "localhost")


async def run(loop):
    """Main loop that the whole party happens in. 
    Listens for messages and handles thru callbacks
    """

    nc = NATS()
    r = redis.Redis(host=REDIS_HOSTNAME, port=6379, db=0)

    async def closed_cb():
        print("Connection to NATS is closed.")
        # await asyncio.sleep(0.1, loop=loop)
        # loop.stop()

    options = {"servers": NATS_SERVERS, "loop": loop, "closed_cb": closed_cb}

    await nc.connect(**options)
    logging.info(f"Connected to NATS at {nc.connected_url.netloc}...")

    async def execution_handler(msg):
        log_feed_message(msg)

        execution = execution_pb2.execution()
        execution.ParseFromString(msg.data)

        key = build_execution_uid(execution)
        data = msg.data
        r.set(key, data)
        logging.info(f"Saved: [{key}]")

    async def sport_event_handler(msg):
        log_feed_message(msg)

        event = event_pb2.event()
        event.ParseFromString(msg.data)

        key = build_sport_uid(event)
        data = msg.data
        r.set(key, data)
        logging.info(f'Saved: [{key}]')

    async def chat_event_handler(msg):
        log_feed_message(msg)
        key = slugify(msg.data)
        data = msg.data.decode()
        r.set(key, data)
        logging.info(f'Saved: [{key}]')

    logging.info(f'Listening for [{EXECUTION_SUBJECT_NAME}]')
    await nc.subscribe(
        EXECUTION_SUBJECT_NAME, EXECUTION_WORKERS_NAME, execution_handler
    )
    logging.info(f'Listening for [{SPORT_EVENT_SUBJECT_NAME}]')
    await nc.subscribe(
        SPORT_EVENT_SUBJECT_NAME, SPORT_EVENT_WORKERS_NAME, sport_event_handler
    )

    logging.info(f'Listening for [{CHAT_EVENTS_SUBJECT_NAME}]')
    await nc.subscribe(
        CHAT_EVENTS_SUBJECT_NAME, CHAT_EVENTS_WORKERS_NAME, chat_event_handler
    )


    # TODO: Need to alsp listen to and store messages from the the chat servers

    # TODO: fix this to work more elegantly. Often hangs
    # when it all eventually ends
    # def signal_handler():
    #     if nc.is_closed:
    #         return
    #     print("Disconnecting...")
    #     # loop.create_task(nc.close())
    #     nc.close()
    # for sig in ('SIGINT', 'SIGTERM'):
    #     loop.add_signal_handler(getattr(signal, sig), signal_handler)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run(loop))
        loop.run_forever()
    finally:
        loop.close()
