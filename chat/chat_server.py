

import asyncio
import json
import logging
import websockets
import signal
import os

from nats.aio.client import Client as NATS

from google.protobuf.json_format import MessageToJson, MessageToDict

from protobuf.generated import event_pb2, execution_pb2
from tools import log_feed_message

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s: %(message)s", datefmt="%H:%M:%S",)

"""Core component that brings together chat room capabilities NATS integration

Environments variables expected:
* NATS_HOSTNAME (default: localhost)
* REDIS_HOSTNAME (default: localhost)
* SOCKET_SERVER_HOSTAME (default: localhost)

File structure:
* handle_exception(loop, context):
* shutdown(loop, signal=None):
* notify_room(message, connections=None):
* handle_socket_connection(websocket, path):
* nats_listener(loop):
    * closed_cb():
    * execution_handler(msg):
    * sport_event_handler(msg):
    * room_events_handler(msg):
    # def signal_handler():
* main():

TODO: Restructure this code into a class structure. Its a mess of nested functions
"""

# TODO: Need to move these to a better place
SOCKET_SERVER_HOSTAME = os.environ.get('SOCKET_SERVER_HOSTAME', '0.0.0.0')
SOCKET_SERVER_PORT = 6789

NATS_HOSTNAME = os.environ.get('NATS_HOSTNAME', 'localhost')
NATS_SERVERS = [f'nats://{NATS_HOSTNAME}:4222']

EVENT_SUBJECT_NAME = 'sport_event'
EXECUTION_SUBJECT_NAME = 'execution'

ROOM_EVENTS_SUBJECT_NAME = 'chatroom_event'


# TODO: Globals! Please fix me.
clients = {}
nats_client = NATS()


def handle_exception(loop, context):
    """logs any exception events and initaites the shutdown process"""
    """"""
    msg = context.get("exception", context["message"])
    logging.error(f"Caught exception: {msg}")
    logging.info("Shutting down...")
    asyncio.create_task(shutdown(loop))

async def shutdown(loop, signal=None):
    """Wraps up, closes open connects and initates task cancels"""
    if signal:
        logging.info(f"Received exit signal {signal.name}...")

    logging.info("Closing database connections...")
    # TODO: close nats connections
    # TODO: close redis connections

    logging.info("Cancelling active tasks...")
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
    [task.cancel() for task in tasks]
    await asyncio.gather(*tasks, return_exceptions=True)

    loop.stop()


async def notify_room(message, connections=None):
    """Sends annoucements to all connected chatters"""
    connections = connections or clients.keys()
    for c in connections:
        await c.send(message)
    # TODO: use gather??
    # await asyncio.wait([c.send(message) for c in connections])
    # TODO: Save this to DB


async def handle_socket_connection(websocket, path):
    """Handles the whole lifecycle of a chatter's websocket connection.
    Consumes and return data as JSON data structure.
    Processes 'enter' and 'chat' messages. 
    Messages relevant to others are put on the NATS queues.
    """

    logging.info(f'New socket connection: {websocket}')

    msg_count = 0
    try:
        # This loop will keep listening on the socket until its closed. 
        async for raw_message in websocket:
            msg_count += 1
            logging.info(f'Got: [{raw_message}]')

            message_data = json.loads(raw_message)

            action = message_data.get('action')
            if not action:
                print('"action" is missing from {message_data}')
                continue

            if action == 'enter':
                name = message_data['name']
                clients[websocket] = name
                logging.info(f'Enter: [{name}] via [{websocket}]')

                # Only the new chatter needs to see the welcome messages so they
                # are sent back directly on socket, not thru nats
                welcome_messages = [
                    f'Welcome: {name}', 
                    f'There are {len(clients)} other users connected: {list(clients.values())}'
                ]
                for m in welcome_messages:
                    await websocket.send(m)

                # Everyone needs to see this annoucement so its sent to nats.
                room_event = {
                    'event_type': 'announcement',
                    'room': 'room1',
                    'message': f'{name} just entered the room.'
                }
                await nats_client.publish(ROOM_EVENTS_SUBJECT_NAME, json.dumps(room_event).encode())


            elif action == 'chat':
                # TODO: ensure the user is in the room?
                msg = message_data['message']
                name = clients[websocket]
                room_event = {
                    'event_type': 'chat',
                    'room': 'room1',
                    'message': f'{name}: {msg}'
                }
                await nats_client.publish(ROOM_EVENTS_SUBJECT_NAME, json.dumps(room_event).encode())

            elif action == 'feed':
                # open door to ingesting 'feed' messages. 
                # Not used because 'feed' data comes in thru NATS
                data = message_data['data']
                await notify_room(f'{data}')

            else:
                logging.warning(f'Ignoring unknown action {action} from [{message_data}]')
                pass

    finally:
        # Connection is closed (for whatever reason)
        name = clients.get(websocket)
        # only clients that started the connection with an 'enter' message get on the clients list
        # the feed's connection is never put on that list
        if name:
            del clients[websocket]
            room_event = {
                'event_type': 'announcement',
                'room': 'room1',
                'message': f'{name} just left the room.'
            }
            await nats_client.publish(ROOM_EVENTS_SUBJECT_NAME, json.dumps(room_event).encode())

        logging.info(f'Processed [{msg_count}] messages from [{websocket}]')


async def nats_listener(loop):
    """Core NATS integration component. Listens for subjects 
    'execution', 'sport', 'room_events'
    """

    async def closed_cb():
        logging.info("Connection to NATS is closed.")
        loop.create_task(nats_client.close())

    options = {
        "servers": NATS_SERVERS,
        "loop": loop,
        "closed_cb": closed_cb
    }

    await nats_client.connect(**options)
    logging.info(f"Connected to NATS at {nats_client.connected_url.netloc}...")

    async def execution_handler(msg):
        # Deseriaizes a protobuf exec messages into JSON and passes on
        log_feed_message(msg)
        execution = execution_pb2.execution()
        execution.ParseFromString(msg.data)
        logging.info(MessageToDict(execution))
        await notify_room(f'{execution}')

    async def sport_event_handler(msg):
        # Deseriaizes a protobuf sport messages into JSON and passes on
        log_feed_message(msg)
        event = event_pb2.event()
        event.ParseFromString(msg.data)
        logging.info(MessageToDict(event))
        await notify_room(f'{event}')

    async def room_events_handler(msg):
        # Captures room events from NATS and notifies connected chatters
        log_feed_message(msg)
        room_event_data = json.loads(msg.data)
        logging.info(room_event_data)
        message = room_event_data['message']
        await notify_room(f'{message}')

    await nats_client.subscribe(EXECUTION_SUBJECT_NAME, cb=execution_handler)
    await nats_client.subscribe(EVENT_SUBJECT_NAME, cb=sport_event_handler)

    await nats_client.subscribe(ROOM_EVENTS_SUBJECT_NAME, cb=room_events_handler)

    # TODO: Fix this shutdown handling. Sometimes hangs.
    # when it ends
    # def signal_handler():
    #     if nats_client.is_closed:
    #         return
    #     logging.info("Disconnecting...")
    #     loop.create_task(nats_client.close())


def main():
    loop = asyncio.get_event_loop()

    signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
    for s in signals:
        loop.add_signal_handler(
            s, lambda s=s: asyncio.create_task(shutdown(loop, signal=s))
        )
    loop.set_exception_handler(handle_exception)

    try:
        socket_server = websockets.serve(handle_socket_connection, SOCKET_SERVER_HOSTAME, SOCKET_SERVER_PORT)
        logging.info(f'Starting socket server: {socket_server} ...')
        loop.run_until_complete(socket_server)

        loop.run_until_complete(nats_listener(loop))

        loop.run_forever()
    finally:
        loop.close()
        logging.info(f"Successfully shutdown [{loop}].")



if __name__ == "__main__":
    main()

